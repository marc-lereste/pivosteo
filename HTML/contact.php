<!DOCTYPE HTML>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/CSS/styleSheet.css" rel="styleSheet">
    <script src="/bootstrap/js/bootstrap.min.js"></script>

    <title> Contact </title>

</head>

<body>
<div id="wrap">
    <div id="header">
        <div class="container-fluid">
            <header class="row text-center ">
                <div class="col-xs-12">
                    <h1> PRISCILLA PIVOST  <br> Ostéopathe D.O <br> 06.44.10.86.82 <br> 01.69.20.00.31
                        <hr style="height: 2px; color: purple; background-color: purple; width: 25%; border: none;">
                    </h1>
                    <div class=" dropdown">
                        <a class="btn" href="index.html">Accueil</a>
                        <a class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
                            Osteopathie
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="osteopathe.html">Votre Ostéopathe</a></li>
                            <li><a href="consulter.html">Pourquoi consulter?</a></li>
                            <li><a href="tarifs_et_remboursements.html">Tarifs et Remboursements</a></li>
                            <li><a href="horaires.html">Horaires</a></li>
                        </ul>
                        <a class="btn" href="Acces.html">Accès</a>
                        <a class="btn" href="contact.php">Contact</a>
                    </div>
                    <br>
                </div>
            </header>
        </div>
        <br>
    </div>

    <div class="contenu_contact">
        <form id="contact" method="post" action="traitement.php" class="form-horizontal panel-body">
            <div class="control-group">
                <legend>Me contacter</legend>
                <label for="nom" class="control-label">Nom</label>
                <div class="controls">
                    <input type="text" id="nom" name="nom">
                </div>
            </div>

            <div class="control-group">
                <label for="email" class="control-label">E-Mail</label>
                <div class="controls">
                    <input type="text" id="email" name="email">
                </div>
            </div>

            <div class="control-group">
                <label for="objet" class="control-label">Objet</label>
                <div class="controls">
                    <input type="text" id="objet" name="objet">
                </div>
            </div>

            <div class="control-group">
                <label for="message"  class="control-label">Votre message</label>
                <div class="controls">
                    <textarea cols="40" name="message"></textarea>
                </div>
            </div>
            <br>
            <button class="btn"<input type="submit" name="envoi" value="Envoyer le Message">Envoyez</button>
        </form>
    </div>

    <div id ="footer">
        <div class="container-fluid">
            <footer class="row text-center">
                <div class="conteneur_footer">
                    <div class="adresse">
                        <address>
                            <br>
                            <strong>PRISCILLA PIVOST<br>Ostéopathe D.O</strong><br>
                            <abbr title="Phone"></abbr> 06.44.10.86.82<br>01.69.20.00.31<br>
                            2 rue Fernand Léger<br>
                            91 320 Wissous<br>
                        </address>
                    </div>
                    <div id="logo">
                        <img src="/IMG/logo.jpg" width="220" height="150"/>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</div>
</body>
</html>


