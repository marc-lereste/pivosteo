<?php

class Date{
    var $days = array('Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi','Dimanche');
    var $months = array('Janvier','Fevrier','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Decembre');

    function getEvents($year){
        global $DB;
        $req = $DB->query('SELECT id,titre,date,heure FROM events WHERE YEAR(date)='.$year);
        $r = array();
        /**
         * Ce que je veux $r[TIMESTAMP][id] = title
         */
        while($d = $req->fetch(PDO::FETCH_OBJ)){
            $r[strtotime($d->date)][$d->id] =$d->titre .' : ' .$d->heure;
        }
        return $r;


    }



    function getAll($year)
    {
        $r = array();
        /**
         * boucle version procedurale
         *
         * $date = strtotime($year.'-01-01');
         * while(date('Y',$date) <= $year) {
         * // CE QUE JE VEUX => $r[ANNEE][MOIS][JOUR] = JOUR DE LA SEMAINE
         * $y = date('Y', $date);
         * $m = date('n', $date);
         * $d = date('j', $date);
         * $w = str_replace('0', '7', date('w', $date));
         * $r[$y][$m][$d] = $w;
         * $date =strtotime(date('y-m-d,$date').'+1 DAY') +24 * 3600;
         * }
         *
         *
         */
        $date = new DateTime($year . '-01-01');
        while ($date->format('Y') <= $year) {
            // CE QUE JE VEUX => $r[ANNEE][MOIS][JOUR] = JOUR DE LA SEMAINE
            $y = $date->format('Y');
            $m = $date->format('n');
            $d = $date->format('j');
            $w = str_replace('0', '7', $date->format('w'));
            $r[$y][$m][$d] = $w;
            $date->add(new DateInterval('P1D'));
        }
        return $r;
    }
}