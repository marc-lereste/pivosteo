<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <link href="../CSS/styleSheet2.css" rel="styleSheet">
    <script type="text/javascript">
        <?php $mois_cours = date('n');?>
        jQuery(function($){
            var monthCours = <?=$mois_cours;?>;
            $('.month').hide();
            $('#month'+monthCours+'').show();
            $('.months a#linkMonth'+monthCours).addClass('active');
            var current = monthCours;
            $('.months a').click(function(){
                var month = $(this).attr('id').replace('linkMonth','');
                if(month != current){
                    $('#month'+current).slideUp();
                    $('#month'+month).slideDown();
                    $('.months a').removeClass('active');
                    $('.months a#linkMonth'+month).addClass('active');
                    current = month;
                }
                return false;
            });
        });
    </script>

<title>Agenda</title>
</head>

<body>
<div class="wrap">
    <header>
<?php
require('date.php');
require ('config.php');
$date = new Date();
$year = date('Y');
$dates = $date->getAll($year);
$events = $date->getEvents($year);
?>
<div class="periods">
    <div class="year"><?php echo $year; ?></div>
    <div class="months">
        <br>
        <br>
        <ul>
            <?php foreach ($date->months as $id => $m): ?>
                <li><a href="#" id="linkMonth<?php echo $id + 1; ?>"><?php echo utf8_encode(substr(utf8_decode($m), 0, 3)); ?></a></li>
            <?php endforeach; ?>
        </ul>
    </div>
    <div class="clear"></div>
    <?php $dates = current($dates); ?>
    <?php foreach ($dates as $m => $days): ?>
    </header>


        <div class="month relative" id="month<?php echo $m; ?>">
            <br>
            <table>
                <thead>
                <tr>
                    <?php foreach ($date->days as $d): ?>
                        <th><?php echo substr($d,0,8); ?></th>
                    <?php endforeach; ?>
                </tr>
                </thead>

                <tbody>
                <tr>
                    <?php $end = end($days); foreach($days as $d => $w): ?>
                    <?php $time = strtotime("$year-$m-$d"); ?>
                    <?php if ($d == 1): $nb=$w-1; if($nb>0): ?>
                        <td colspan="<?php echo $nb; ?>" class="padding"></td>
                    <?php endif; ?>
                    <?php endif; ?>
                    <td<?php if ($time == strtotime(date('Y-m-d'))): ?> class="today" <?php endif; ?>>
                        <div class="relative">
                            <div class="day"><?php echo $d; ?>
                            </div>
                        </div>
                        <div class="daytitle">
                            <?php echo $date->days[$w-1] ?>	<?php echo $d ?> <?php echo $date->months[$m-1] ?>

                        </div>
                        <ul class="events">

                            <?php if (isset ($events[$time])): foreach($events[$time] as $e):?>
                                <li ><?php echo $e ?>	</li>
                            <?php endforeach; endif; ?>

                        </ul>

                    </td>
                    <?php if ($w == 7): ?>
                </tr>
                <tr>
                    <?php endif; ?>
                    <?php endforeach; ?>
                    <?php if ($end != 7): ?>
                        <td colspan="<?php echo 7 - $end; ?>" class="padding"></td>
                    <?php endif; ?>
                </tr>
                </tbody>
            </table>
        </div>
    <?php endforeach; ?>
</div>
</div>
</body>
</html>